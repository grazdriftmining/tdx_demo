function [ds] = datagenerator(confName,seed)
% rng seed
if ~exist('seed','var')
    seed = 1;
end
rng(seed)

run(strcat('config/',confName,'.m'))
X = [];
C = [];
T = [];
tStart = 0;
tBins = (tStart:(round(segmentLength*nSegments*10)-1))*0.1;
pds = cell(length(components),nSegments);
for iSegment=1:nSegments
    Xs = [];
    Cs = [];
    Ts = [];
    nSegSamples = round(nSamples * (mixtureCoefs(:,iSegment) .* segDataPerc(iSegment)));
    for iComp=1:length(components)
       pd = [];
       if strcmp(components{iComp,1},'F')
           pd = FDist(params{iComp,iSegment}{:});
       elseif strcmp(components{iComp,1},'SkewNormal')
           pd = SkewNormal(params{iComp,iSegment}{:});
       else
           pd = makedist(components{iComp,1},params{iComp,iSegment}{:});
       end
       if exist('distSupport','var')    % if the config includes support, truncate the pd
           if (size(distSupport,1)==length(components)) && (size(distSupport,2)==nSegments)
               pd = truncate(pd,distSupport{iComp,iSegment}{1},distSupport{iComp,iSegment}{2});
           else
               pd = truncate(pd,distSupport(1),distSupport(2));
           end
       end
       sampledX = random(pd, nSegSamples(iComp),dim);
       Xs = vertcat(Xs,sampledX);
       pds{iComp,iSegment} = pd;
%        Ts = vertcat(Ts,sort(((tStart+segmentLength)-tStart).*rand(size(sampledX,1),1) + tStart));
       Ts = vertcat(Ts,repmat((iSegment-1)*segmentLength,nSegSamples(iComp),1));
       Cs = vertcat(Cs,repmat(classLabels(iComp),nSegSamples(iComp),1));
    end
    
    X = vertcat(X,Xs);
    C = vertcat(C,Cs);
    T = vertcat(T,Ts);
    tStart = iSegment*segmentLength;
end
% scatter(T,X,[],C)
[T, sortOrder] = sort(T);
X = X(sortOrder,:);
C = C(sortOrder,:);
ds = DataStream(confName,dim,X,C,T,nSegments,segmentLength,components,params,mixtureCoefs,pds);
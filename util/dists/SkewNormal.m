classdef SkewNormal < ProbDist
    % https://en.wikipedia.org/wiki/Skew_normal_distribution
    % Class implementing the Skew Normal distribution. RNG is using rejection sampling
    
    properties
        xi
        omega
        alpha
        xTrans
        isTruncated
    end
    
    methods
        function obj = SkewNormal(varargin)
            %SkewNormal Construct an instance of this class
            % xi    - real, location
            % omega - pos. real, scale
            % alpha - real, shape
            
            obj.DistName = 'SkewNormal';
            obj.InputData = [];
            obj.Support = [-Inf,Inf];
            obj.isTruncated = false;
            
            % Parameters
            p = inputParser;
            paramName1 = 'xi';
            defaultVal1 = 1;
            paramName2 = 'omega';
            defaultVal2 = 1;
            paramName3 = 'alpha';
            defaultVal3 = 1;
            addParameter(p,paramName1,defaultVal1)
            addParameter(p,paramName2,defaultVal2)
            addParameter(p,paramName3,defaultVal3)
            
            p.parse(varargin{:})
            obj.xi = p.Results.xi;
            obj.omega = p.Results.omega;
            obj.alpha = p.Results.alpha;
            obj.xTrans = @(X)(X-obj.xi)/obj.omega; % for simplicity's sake with later calls
        end

        
        function Y = cdf(obj,X)
%             Y = cdf('Normal',obj.xTrans(X),0,1) - 2.*owenT(obj.xTrans(X),obj.alpha);
            Y = cdf('Normal',obj.xTrans(X),0,1) - 2.*arrayfun(@owenWrapper,obj.xTrans(X));
            function T = owenWrapper(z)
                T = owenT(z,obj.alpha);
            end
        end
        
        function Y = pdf(obj, X)
            if ~obj.isTruncated
                Y = 2/obj.omega .* pdf('Normal',obj.xTrans(X),0,1) .* cdf('Normal',obj.alpha*obj.xTrans(X),0,1);
            else
                a = obj.Support(1);
                b = obj.Support(2);
                gx = (a < X) & (X <= b);
                gx = gx .* (2/obj.omega .* pdf('Normal',obj.xTrans(X),0,1) .* cdf('Normal',obj.alpha.*obj.xTrans(X),0,1));
                Y = gx/(obj.cdf(b) - obj.cdf(a));

            end
        end
        
%         function X = random(obj, M,N)
%             %check how many parameters were passed with varargin
%             if ~exist('M','var')
%                 M = 1;
%             end
%             if ~exist('N','var')
%                 N = 1;
%             end
%             % define function to be used with rejection sampling
%             f = @(x)obj.pdf(x);
%             npd = makedist('normal','mu',obj.xi,'sigma',obj.omega);
%             if obj.isTruncated
%                 npd = truncate(npd,obj.Support(1),obj.Support(2));
%             end
%             g = @(x)2*pdf(npd,x);
%             grnd = @()normrnd(obj.xi,obj.omega);
%             X = accrejrnd(f,g,grnd,0.7,M,N);       
%         end
        function X = random(obj, M,N)
            %check how many parameters were passed with varargin
            if ~exist('M','var')
                M = 1;
            end
            if ~exist('N','var')
                N = 1;
            end
            Mi = 2*M;
            u1=normrnd(0,1,1,Mi,N);
            u2=normrnd(0,1,1,Mi,N);
            id=(u2>obj.alpha*u1);
            u1(id)=-u1(id);
            X=obj.xi+obj.omega.*u1;
            if obj.isTruncated
                X=X(X>obj.Support(1) & X <obj.Support(2));
            end
            X=X(1:M)';
            
        end
        
        function pd = truncate(obj, a, b)
            pd = obj;
            pd.Support = [a,b];
            pd.isTruncated = true;
        end

        function Y = fit()
            % Placeholder that has to be implemented
            % unless fitdist is intended to be used, this can remain blank
        end
    end
end


classdef FDist < ProbDist
    % Class modeling the F-Distribution 
    % https://en.wikipedia.org/wiki/F-distribution
    
    properties
        d1
        d2
        isTruncated
    end
    
    methods
        function obj = FDist(varargin)
            %FDist Construct an instance of this class
            % d1 - pos. real, 1st degree of freedom
            % d2 - pos. real, 2st degree of freedom
            % 
            
            obj.DistName = 'F';
            obj.InputData = [];
            obj.Support = [0,Inf];
            obj.isTruncated = false;
            
            % Parameters
            p = inputParser;
            paramName1 = 'd1';
            defaultVal1 = 1;
            paramName2 = 'd2';
            defaultVal2 = 1;
            addParameter(p,paramName1,defaultVal1)
            addParameter(p,paramName2,defaultVal2)
            p.parse(varargin{:})
            obj.d1 = p.Results.d1;
            obj.d2 = p.Results.d2;
        end
        
        function Y = cdf(obj,X)
            if ~obj.isTruncated
                Y = fcdf(X, obj.d1, obj.d2);
            else
                Y = (fcdf(X,obj.d1,obj.d2) - fcdf(obj.Support(1),obj.d1, obj.d2))/(fcdf(obj.Support(2),obj.d1, obj.d2)-fcdf(obj.Support(1),obj.d1, obj.d2));
            end
        end
        
        function Y = icdf(obj, p)
            Y = finv(p, obj.d1, obj.d2);
        end
        
        function Y = iqr(obj) 
            Y = obj.icdf(0.75) - obj.icdf(0.25);
        end
        
        function Y = pdf(obj, X)
            if ~obj.isTruncated
                Y = fpdf(X, obj.d1, obj.d2);
            else
                a = obj.Support(1);
                b = obj.Support(2);
                gx = (a < X) & (X <= b);
                gx = gx .* fpdf(X,obj.d1, obj.d2);
                Y = gx/(obj.cdf(b) - obj.cdf(a));
            end
        end
        
        
        function X = random(obj, M,N)
            %check how many parameters were passed with varargin
            if ~exist('M','var')
                M = 1;
            end
            if ~exist('N','var')
                N = 1;
            end
            if ~obj.isTruncated
                X = frnd(obj.d1,obj.d2, M, N);
            else
                f = @(x)obj.pdf(x);
                g = @(x)fpdf(x,obj.d1,obj.d2);
                grnd = @()frnd(obj.d1,obj.d2);
                X = accrejrnd(f,g,grnd,0.7,M,N);
            end
        end
        
        function pd = truncate(obj, a, b)
            pd = obj;
            pd.Support = [a,b];
            pd.isTruncated = true;
        end
        
        function Y = fit()
            % Placeholder that has to be implemented
            % unless fitdist is intended to be used, this can remain blank
        end
    end
end


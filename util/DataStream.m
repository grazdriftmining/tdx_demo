classdef DataStream
    % DataStream this object encapsulates the information of a data set in
    % a stream form. For all data sets it contains information like the
    % data set name (dname), the feature (X), the class labels (C, if available)
    % and the time (T). 
    % For artificial da ta sets it also contains the data generating
    % process as passed by the datagenerator. That's why for these data
    % sets the pdf-method is included.
    
   properties
      dname
      dim
      X
      C
      T
      nSegments
      segmentLength
      components 
      parameters % the parameters of the different components in the mixture distribution
      mixtureCoefs % the mixture weights of the different PDs of artificial data
      pds % the probability distribution objects of the artificial data
      featurenames % string array of feature names
   end
   methods
       function obj = DataStream(dname,dim,X,C,T,nSegments,segmentLength,components,parameters,mixtureCoefs,pds,featurenames)
         obj.dname = dname;
         obj.dim = dim;
         obj.nSegments = nSegments;
         obj.segmentLength = segmentLength;
         obj.X = X;
         obj.C = C;
         if length(T)==length(X)
             obj.T = T;
         else
            error('X and T must have the same length')
         end
         obj.components = components;
         obj.parameters = parameters;
         obj.mixtureCoefs = mixtureCoefs;
         obj.pds = pds;
         if ~exist('featurenames','var')
             S = strings(obj.dim,1);
             for si=1:length(S)
                 S(si)=string(strcat('X',num2str(si)));
             end
             obj.featurenames = S;
         else
             obj.featurenames = featurenames;
         end
       end
       function F = cdf(obj,x,t,c)
            % evaluate the mixture distribution for X and T
            % c is an optional vector of indices, indicating which
            % components should be evaluated
           if ~exist('c','var')
                c = 1:size(obj.parameters,1);
            end
            F = zeros(1,length(x));
            tbin = unique(round(t*length(unique(obj.T))));
            if length(tbin)>1
                error('different t in datastream.cdf call')
            end
            for ci=c
                F = F + (obj.mixtureCoefs(ci,tbin)* cdf(obj.pds{ci,tbin},x));
            end
       end
       
       function F = pdf(obj,x,t,c)
            % evaluate the mixture density for X and T
            % c is an optional vector of indices, indicating which
            % components should be evaluated
            F = zeros(length(t),length(x));
            if ~exist('c','var')
                c = 1:size(obj.parameters,1);
            end
            tbin = round(t*length(unique(obj.T)));
            utbin = unique(tbin);
            for ti=1:length(utbin)
                t = utbin(ti);
                f = zeros(1,length(x));
                for ci=c
                    f = f + (obj.mixtureCoefs(ci,t)* pdf(obj.pds{ci,t},x));
                end
                F(tbin==t,:)=repmat(f,length(find(tbin==t)),1);
            end
       end
   end
end
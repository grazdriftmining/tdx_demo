classdef DensityEstimator
    %DensityEstimator generic interface-like superclass to help with the
    % KDD experiments
      
    methods (Abstract)
        fit(X,T)
        pdf(X,T)
        clone(DensityEstimator)
    end
end


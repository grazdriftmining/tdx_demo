dim = 1;
nSamples=25000;
nSegments=120;
distSupport = [0,10];
components = {'SkewNormal';'SkewNormal';'SkewNormal'};
% rows = components; columns = segments
mixtureCoefs = repmat(1/length(components),length(components),nSegments); % mixture weights remain the same

      
% params need to be nComponents x nSegments cell array
location = [repmat(1,1,nSegments);
            repmat(4,1,nSegments);
            repmat(7.5,1,nSegments)
            ];  
scale = [linspace(0.5,1.5,nSegments); 
        linspace(1.7,0.7,nSegments);
        linspace(0.5,1.6,nSegments)
        ];
shape = [repmat(2,1,nSegments);
        repmat(1,1,nSegments);
        repmat(-0.8,1,nSegments);
        ];

params = cell(length(components),nSegments);
for s=1:nSegments
    for c=1:length(components)
        params{c,s} = {'xi',location(c,s),'omega',scale(c,s),'alpha',shape(c,s)};
    end
end

segmentLength = 1/nSegments;
segDataPerc = repmat(1/nSegments,1,nSegments);
classLabels = 1:length(components);



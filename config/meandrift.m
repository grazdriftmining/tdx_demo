dim = 1;
nSamples=25000;
nSegments=120;
distSupport = [0,12];
components = {'SkewNormal';'SkewNormal';'SkewNormal';'SkewNormal'};
% rows = components; columns = segments
mixtureCoefs = repmat(1/4,4,nSegments); % mixture weights remain the same

      
% params need to be nComponents x nSegments cell array
location = [ones(1,nSegments);
            linspace(4,2.5,nSegments);
            linspace(6,8,nSegments);
            linspace(9,8.5,nSegments)
            ];  
scale = [ones(1,nSegments); 
        ones(1,nSegments)*0.7;
        ones(1,nSegments);
        ones(1,nSegments)
        ];
shape = [linspace(3,0,nSegments);
        linspace(2,-1,nSegments);
        repmat(0.8,1,nSegments);
        repmat(2,1,nSegments)
        ];

params = cell(length(components),nSegments);
for s=1:nSegments
    for c=1:length(components)
        params{c,s} = {'xi',location(c,s),'omega',scale(c,s),'alpha',shape(c,s)};
    end
end

segmentLength = 1/nSegments;
segDataPerc = repmat(1/nSegments,1,nSegments);
classLabels = 1:length(components);



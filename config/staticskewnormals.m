dim = 1;
nSamples=25000;
nSegments=120;
distSupport = [0,11];
components = {'SkewNormal';'SkewNormal';'SkewNormal';'SkewNormal'};
% rows = components; columns = segments
mixtureCoefs = repmat(1/4,4,nSegments); % mixture weights remain the same

      
% params need to be nComponents x nSegments cell array
location = [ones(1,nSegments);
            repmat(4,1,nSegments);
            repmat(6,1,nSegments);
            repmat(9,1,nSegments)
            ];  
scale = [ones(1,nSegments); 
        ones(1,nSegments);
        ones(1,nSegments);
        ones(1,nSegments)
        ];
shape = [repmat(3,1,nSegments);
        repmat(2,1,nSegments);
        repmat(0.8,1,nSegments);
        repmat(2,1,nSegments)
        ];

params = cell(length(components),nSegments);
for s=1:nSegments
    for c=1:length(components)
        params{c,s} = {'xi',location(c,s),'omega',scale(c,s),'alpha',shape(c,s)};
    end
end

segmentLength = 1/nSegments;
segDataPerc = repmat(1/nSegments,1,nSegments);
classLabels = 1:length(components);



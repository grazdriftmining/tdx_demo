dim = 1;
nSamples=25000;
nSegments=120;
distSupport = [0,7];
components = {'SkewNormal';'SkewNormal';'SkewNormal'};
% rows = components; columns = segments
mixtureCoefs = [linspace(0.1,0.45,nSegments);
                linspace(0.7,0.1,nSegments)
    ];
mixtureCoefs(3,:) = 1-sum(mixtureCoefs(1:2,:),1);

      
% params need to be nComponents x nSegments cell array
location = [repmat(1,1,nSegments);
            repmat(4,1,nSegments);
            repmat(5,1,nSegments);
            ];  
scale = [ones(1,nSegments)*0.7; 
        ones(1,nSegments)*0.6;
        ones(1,nSegments)
        ];
shape = [repmat(1,1,nSegments);
        repmat(-0.5,1,nSegments);
        repmat(1.5,1,nSegments)
        ];

params = cell(length(components),nSegments);
for s=1:nSegments
    for c=1:length(components)
        params{c,s} = {'xi',location(c,s),'omega',scale(c,s),'alpha',shape(c,s)};
    end
end

segmentLength = 1/nSegments;
segDataPerc = repmat(1/nSegments,1,nSegments);
classLabels = 1:length(components);



% % 
%   Demo for TDX, generating weightdrift-pattern data and fitting a tdx
%   model. Tested with Matlab R2017b.
% % 
close all
addpath('tdx','util','util/dists','util/owenT')

% generate the data set, construct a point set for density evaluation later
ds = datagenerator('weightdrift');
xGrid = linspace(quantile(ds.X,0.01),quantile(ds.X,0.99),200);

% designate training and test set
train_idx = 1:ceil(0.66*size(ds.X,1));
x_train = ds.X(train_idx);
t_train = ds.T(train_idx);
test_idx = ceil(0.66*size(ds.X,1))+1:size(ds.X,1);
x_test = ds.X(test_idx);
t_test = ds.T(test_idx);

model = tdx(14,0.6,5,2).fit(x_train,t_train); % fit tdx model to training data
gamma = model.get_Gamma(ds.T); % compute the basis weights (gamma) for
                                %visualization purposes

pred_dens = model.pdf(xGrid,t_test); % extrapolate to test window & evaluate on xGrid
true_dens = ds.pdf(xGrid,t_test); % compute the true density 

% % 
%  Plotting block
% % 

% plot the data with basis function locations
figure(1)
subplot(2,1,1)
hold on
scatter(ds.T,ds.X,[],ds.C)
scatter(zeros(size(model.Mu)),model.Mu,[],'r^')
title('Scatter plot of the data')
xlabel('T')
ylabel('X')
legend({'observation','basis'},'Location','eastoutside')

% plot the basis weights as area plot
subplot(2,1,2)
cols = hsv(length(model.Mu));
h=area(ds.T,gamma);
title('Basis Weights over time')
xlabel('T')
xlim([0,1])
ylim([0,1])
leg = cell(1,length(model.Mu));
for i=1:length(model.Mu)
    leg{i}=num2str(round(model.Mu(i),2));
    h(i).FaceColor = cols(i,:);
end
legend(leg,'Location','eastoutside')
set(gcf,'pos',[10 10 960 1080])
hold off

% plot predicted and true density
figure(2)
timeidxs = [1,round(length(find(test_idx))/2),length(find(test_idx))];
for ti=1:length(timeidxs)
    subplot(length(timeidxs),1,ti)
    plot(xGrid,true_dens(timeidxs(ti),:),'-b',xGrid,pred_dens(timeidxs(ti),:),'--r')
    title(strcat('Density at t=',num2str(t_test(timeidxs(ti)))))
    xlabel('X')
    ylabel('P(X)')
end
set(gcf,'pos',[960 10 960 1080])

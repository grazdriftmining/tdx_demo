Brief readme for the TDX code archive.
Note: do not redistribute without permission by Georg or me,

The code is segmented into several portions, taken from the current (November 2018) project repository. In case there is anything missing or if there are any questions, please contact me directly.

Structure:

./ contains only the DensityEstimator interface
data/ contains the .mat files of the data sets we used in the form of DataStream objects
	data/true_densities/ contains the "true"/baseline densities of the real-world data sets, based on an ensemble of smoothed histograms
tdx/ contains both tdx as proposed in the paper as well as its static version, which only fits the basis expansion model. Each of these classes have a separate fitting function in a separate file.
util/ contains some utility functions, I only selected the DataStream class and a utility to convert DataStream objects to csv here. 


Usage:

- create a tdx-object with the desired parameters
- fit the model via the fit(...) function. Default starting points for the optimization problem is set to 1 here - this can be edited in case you get the impression the solution is not satisfying
- to estimate the density of a sample given as two vectors X and T, simply call the pdf(...) function of the fitted tdx object with these vectors as parameters.
- in case the basis weights are of interest, call get_Gamma(...) of a fitted tdx object and pass it a vector of time points for which you want the weights. 


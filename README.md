# Temporal Density Extrapolation Demo

This repository contains a small demo to showcase the temporal density extrapolation (tdx) method proposed 
in ["Temporal density extrapolation using a dynamic basis approach"](https://link.springer.com/article/10.1007/s10618-019-00636-0)
that generates an artificial data set, fits a tdx model to part of it and predicts the density for the rest of the data. 
Five plots are produced, showing the data, the basis weights of the model as well as the true and predicted density at 3 points within the test window.


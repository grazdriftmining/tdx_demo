classdef tdx_static < DensityEstimator
    properties
        M
        h
        Mu
        G
    end
    
    methods
        function obj = tdx_static(M,h)
            obj.M = M;
            obj.h = h;
            
        end
        %% model fitting
        function obj =  fit(obj,xTrain,tTrain)            
            obj.Mu = linspace(quantile(xTrain,0.01),quantile(xTrain,0.99),obj.M);
            obj.G = tdxFit_static(xTrain,obj.Mu,obj.h);
        end
        
        %% Model density
        function f = pdf(obj,X,T)
            Phi = normpdf(X,obj.Mu,obj.h);
            f = sum(Phi .* obj.G,2);
        end
        
        %% clone function - duplicate the configured estimator without its fit
        function cobj = clone(obj)
            cobj = tdx_static(obj.M,obj.h);
        end
        
    end
end


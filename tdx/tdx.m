classdef tdx < DensityEstimator
    % TDX stream density estimator as described in the paper
    %   Inherits from DensityEstimator to make it work with the
    %   component-based framework for KDD
    
    properties
        M % int, number of pseudo points
        h % float, bandwidth of the Gaussian PDF
        R % int, order of the fitted polynomials
        L % int, regularization factor
        Mu % Mean-Parameters of the basis functions (i.e., their location)
        A % Coefficient matrix
        U % transformation matrix
    end
    
    methods
        function obj = tdx(M,h,R,L)
            p = inputParser;
            addRequired(p,'M',@(x) isnumeric(x) & x>0);
            addRequired(p,'h',@(x) isnumeric(x) & x>0);
            addOptional(p,'R',3,@(x) isnumeric(x) & x>0);
            addOptional(p,'L',1,@(x) isnumeric(x) & x>=0);
            parse(p,M,h,R,L);

            obj.M = M;
            obj.h = h;
            obj.R = R;
            obj.L = L;
        end

        %% fit tdx model
        function obj = fit(obj,xTrain,tTrain,varargin)
            p = inputParser;
            addRequired(p,'xTrain',@isvector);
            addRequired(p,'tTrain',@isvector);
            addParameter(p,'nstartpoints',1,@isnumeric);
            addParameter(p,'startpoint',[]); 
            parse(p,xTrain,tTrain,varargin{:});
            nstartpoints = p.Results.nstartpoints;
            startpoint = p.Results.startpoint;
            if ~(length(xTrain)==length(tTrain))
                error('Data vectors must be of equal length')
            end
            obj.Mu = linspace(quantile(xTrain,0.01),quantile(xTrain,0.99),obj.M);
            [obj.A,obj.U] = tdxFit(xTrain,tTrain,obj.Mu,obj.h,obj.R,obj.L,...
                'nstartpoints',nstartpoints,'startpoint',startpoint);
        end
        
        %% compute basis weights
        function G = get_Gamma(obj, T, varargin)
            % varargin - optional parameters if specific A and U should be
            % used. Structure of varargin is {A, U}
            if isempty(varargin)
                % compute basis weights for a vector of time values T
                a = repmat(T,1,obj.R+1).^repmat(0:obj.R,size(T,1),1);
                eMat = exp(obj.U*obj.A*a');
                I = ones(obj.M,1);
                G = (eMat./(I*sum(eMat,1)))';
            elseif length(varargin)==2
                a = repmat(T,1,obj.R+1).^repmat(0:obj.R,size(T,1),1);
                eMat = exp(varargin{2}*varargin{1}*a');
                I = ones(obj.M,1);
                G = (eMat./(I*sum(eMat,1)))';
            end
        end
        
        %% Model density
        function f = pdf(obj,X,T)
            if ~isequal(size(X),size(T))
                f = zeros(length(T),length(X));
            else
                f = zeros(length(X),1);
            end
            
            G = obj.get_Gamma(T);
            for m=1:obj.M
                if ~isequal(size(X),size(T))
                    % normal density evaluation use case
                    f = f + G(:,m)*normpdf(X,obj.Mu(m),obj.h);
                else
                    % classification use case
                    f= f + G(:,m).*normpdf(X,obj.Mu(m),obj.h);
                end
            end
        end
        
        %% clone function - duplicate the configured estimator without its fit
        function cobj = clone(obj)
            cobj = tdx(obj.M,obj.h,obj.R,obj.L);
        end
    end
end


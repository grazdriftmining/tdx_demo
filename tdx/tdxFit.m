function [A,U] = tdxFit(xTrain,tTrain,Mu,h,R,L,varargin)
%     Function to fit the compositional TDX model
%     Input (required):
%         xTrain (nSamples x 1 array), feature values of the training samples
%         tTrain (nSamples x 1 array), time values of the training samples
%         Mu (1 x M) array, feature values of the pseudo points
%         h (double,positive), spatial bandwidth/standard deviation of the mixture model components
%         R (int,positive), order of the temporal fitting polynomial
%         L (numeric,nonnegative), regularization factor
%     Input (optional, Name-value):
%         nstartpoints (int), number of starting points to be used, default 1....
%             If > 1 Multistart framework is used.
%         startpoint (M-1 x R+1 matrix), starting point for the
%             optimization algorithm
%     Output:
%         A (M-1 x R+1 matrix), coefficients of the TDX model (B in the article)
%         U (M x M-1 matrix), matrix used for the ilr-transformation
% 
    shapecheck = @(x) size(x)==[length(Mu)-1,R+1];% validate M-1,R+1

    p = inputParser;
    addRequired(p,'xTrain',@isvector);
    addRequired(p,'tTrain',@isvector);
    addRequired(p,'Mu',@isvector);
    addRequired(p,'h',@(x) isnumeric(x) & x>0);
    addRequired(p,'R',@(x) isnumeric(x) & x>0);
    addRequired(p,'L',@(x) x>=0);

    addParameter(p,'nstartpoints',1,@isnumeric);
    addParameter(p,'startpoint',[]); 
    parse(p,xTrain,tTrain,Mu,h,R,L,varargin{:});
    
    nstartpoints = p.Results.nstartpoints;
    x = p.Results.startpoint;

    M = length(Mu);
    n = R+1;
    Phi = normpdf(xTrain,Mu,repmat(h,size(Mu))); % compute the basis function matrix
    
    % safety measure: set zero elements of Phi to small non-zero value
    % this should prevent the obj. function to fail, since if any entry of
    % phi is equal zero the computation of the function value includes
    % multiplying by zero. This leads to log(0), which is -Inf.
    Phi(Phi==0)=1e-5;
    
    
    % EXPONENTIAL WEIGHT DECAY
    Tmax = 1;
    halflife=0.1;
    psi = -log(.5)/(halflife*Tmax);
    fw = @(t,psi) exp(-psi.*t);
    W = fw(max(tTrain)-tTrain,psi);
    
    % compute transformation matrix U
    Utilde = zeros(M,M-1)+(-triu(ones(M,M-1)));
    for k=1:(M-1)
        Utilde(k+1,k) = k;
    end
    % Matlab2016b doesn't support the vecnorm function
    vn = zeros(1,size(Utilde,2));
    for ui=1:size(Utilde,2)
        vn(ui) = norm(Utilde(:,ui));
    end
    U = Utilde *(diag(1./vn));
    
    % generate the design matrix of the regression problem
    a = repmat(tTrain,1,R+1).^repmat(0:R,size(tTrain,1),1);
    J = JVect(U,a,M,n); % J is later needed for the objective gradient
    
    % create the D matrix
    D = zeros(n,n-1);
    for k=1:(n-1)
        D(k+1,k) = 1;
    end
    
% % stuff for diff penalty
%     % make L matrix for diff penalty
%     L1 = diag(ones(1,M-1));
%     L2 = diag(ones(1,M-2)*-1,1);
%     L=L1(1:end-1,:)+L2(1:end-1,:);   
%     % make K matrix
%     K = zeros(M-1,M-2);
%     K(1:M-1:end)=1;
%     K(2:M-2:end)=-1;
%     K(M-1,M-2) = -1;
%     z = zeros(n,1); % vector used to reduce the difference matrix
%     z(3) = 1; % order x-1 will be penalized (because of intercept being 1)
% %
    
    oneMat = ones(size(Phi));

    % quasi-newton options
    % if not run on the cluster, I'd advise to set 'UseParallel' to true
    opt = optimoptions('fminunc','SpecifyObjectiveGradient',true,...
        'Algorithm','quasi-newton','Display','notify','CheckGradients',false,...
        'OptimalityTolerance',1e-4,'UseParallel',false);

    rng(32);
    % check or generate startpoint for optimization
    if ~shapecheck(x)
        x = rand(M-1,R+1);
    end
    x = x(:)'; %flatten because optimization framework wants it like that
    try
        if nstartpoints > 1
            % block in case it is desired to use multiple starting points...
            % in the optimization
            stpoints = RandomStartPointSet('NumStartPoints',nstartpoints,'ArtificialBound',2.0);
            problem = createOptimProblem('fminunc','objective',...
            @F,'x0',x,'options',opt);
            ms = MultiStart('UseParallel',false);
            x = run(ms,problem,stpoints);
        else
            % if number of starting points is just 1, only solve the problem
            % once
            x = fminunc(@F,x,opt);
        end
    catch ME
        ME 
        fprintf('Optimization failed, 10 different random points\n')
        retries = 0;
        while retries <= 10
            x = rand(M-1,R+1);
            x = x(:)';
            try
                x = fminunc(@F,x,opt);
                break
            catch ME
                ME
                retries = retries + 1;
                fprintf('Optimization failed, retrying different random point\n')
            end
        end
        if retries == 11
            error('tdxFit could not recover from failure of optimisation problem!')
        end
    end


    
    % Nested function that computes the objective function     
    % returns x that minimizes the function
        function [val,g] = F(x)
            A = reshape(x,R+1,M-1)'; % transform x vector into A matrix
            val = -funVect(Phi,U,A,a,oneMat,L,D,W); % function value
%             val = -funVect_diffpen(Phi,U,A,a,oneMat,W,lambda,L,z,K); 

            if nargout > 1
                g = -gradientVect(Phi,U,A,a,J,oneMat,L,D,W); % gradient
%                 g = -gradientVect_diffpen(Phi,U,A,a,J,oneMat,W,lambda,L,z,K);
            end
        end
    
    
    % in the end we need to rearrange A back into it's original matrix shape
    A = reshape(x,R+1,M-1)';
end


function yt = ytildeVect(y,A,a,U)
    % Vectorized computation of Ytilde for the entire training set
    b = dot(y',exp(U*A*a'))';
    tmp = 1./b .*y;
    tmp(isinf(tmp)) = 0; % some elements of b are 0, therefore we need to handle the resulting Inf cells from the division
%     yt = zeros(length(a),length(U));
%     for j=1:length(a)
%         yt(j,:)= tmp(j,:) * diag(exp(U*A*a(j,:)'));
%     end
    yt = tmp.*exp(U*A*a')'; 

end

function J = JVect(U,a,M,n)
    % Vectorized computation of the jacobians for the entire training set
    Urep = zeros(M,((M-1)*n));
    for it=1:M-1
        if it==1
            Urep = repmat(U(:,it),1,n);
        else
            Urep(:,(size(Urep,2)+1):size(Urep,2)+n) = repmat(U(:,it),1,n);
        end
    end
    
    J = zeros(M,(M-1)*n,length(a));
    for j=1:length(a)
        J(:,:,j)=Urep.*repmat(a(j,:),M,M-1);
    end
    % J is M x (M-1)*n x N matrix
end

function J = JVectblocked(U,a,M,n)
    % Vectorized computation of the (blocked) jacobians for the entire training set
    M = size(U,1);
    n = size(a,2);
    Urep = zeros(M,((M-1)*n));
    for it=1:M-1
        if it==1
            Urep = repmat(U(:,it),1,n);
        else
            Urep(:,(size(Urep,2)+1):size(Urep,2)+n) = repmat(U(:,it),1,n);
        end
    end
    
%     J = zeros(M*length(a),(M-1)*n);
    J = zeros(M,(M-1)*n*length(a));
    
    for j=1:length(a)
%         J(M*j-(M-1):M*j,:)=Urep.*repmat(a(j,:),M,M-1);
        J(:,(((M-1)*n)*j-((M-1)*n-1)):(((M-1)*n)*j))=Urep.*repmat(a(j,:),M,M-1);
    end
    % J is M*N x (M-1)*n block matrix
end

function g = gradientVect(Phi,U,A,a,J,oneMat,L,D,W)
    yt1 = ytildeVect(Phi,A,a,U);
    yt2 = ytildeVect(oneMat,A,a,U);
    
    g = zeros(1,size(J,2));
    for j=1:length(Phi)
        g = g + ((yt1(j,:,:) * J(:,:,j) .* W(j)) - (yt2(j,:,:) * J(:,:,j) .* W(j)));
    end
%     fprintf(strcat('\nOld:',num2str(toc),'s\n'))
%     tic
%     Z1 = zeros(size(a,1),(M-1)*n);
%     Z2 = zeros(size(a,1),(M-1)*n);   
%     for i=1:size(a,1)
%         Z1(i,:) = yt1(i,:)*J(:,(((M-1)*n)*i-((M-1)*n-1)):(((M-1)*n)*i));
%         Z2(i,:) = yt2(i,:)*J(:,(((M-1)*n)*i-((M-1)*n-1)):(((M-1)*n)*i));
%     end
%     g = sum(Z1.*W - Z2.*W);
%     fprintf(strcat('\nNew:',num2str(toc),'s\n'))
    
    
    if L > 0
        penalty = L*A*D*D';
        penalty = reshape(penalty',1,size(A,1)*size(A,2));
        g = g - penalty;
    end
end



function f = funVect(Phi,U,A,a,oneMat,lambda,D,W)
    % D is the penalty specific matrix
    f = 0;
%     tic
%     for j=1:length(Phi)
%         f = f+ (W(j)*log(dot(Phi(j,:),exp(U*A*a(j,:)'))) - W(j)*log(dot(oneMat(j,:),exp(U*A*a(j,:)'))));
%         if isinf(f)
%             fprintf('\ntdx function value equals inf!\n')
%             error('tdx function value equals inf!')
%         end
%     end
%     t =toc;
%     fprintf(strcat('\nold:',num2str(t),'s\n'))
%     tic
    e = exp(U*A*a');    
    f = sum((W'.*log(dot(Phi',e)))-(W'.*log(sum(e))));
    % apply penalization
    if lambda>0
        penalty = lambda * trace((D'*A')*A*D); % coefficient penalty
        f = f - penalty;
    end
end

%% deprecated difference penalty functions

% function g = gradientVect_diffpen(Phi,U,A,a,J,oneMat,W,lambda,L,z,K)
%     yt1 = ytildeVect(Phi,A,a,U);
%     yt2 = ytildeVect(oneMat,A,a,U);
%     g = zeros(1,size(J,2));
%     for j=1:length(Phi)
%         g = g + ((yt1(j,:,:) * J(:,:,j) .* W(j)) - (yt2(j,:,:) * J(:,:,j) .* W(j)));
%     end
%     
%     if lambda > 0
%         dnom = 2.*sqrt(((L*A)*z).^2);
%         ptemp = repmat((K *(2.*((L*A)*z)))',n,1);
%         r=repmat(z,1,M-1);
%         penalty = reshape((ptemp.*r),1,n*(M-1));
% 
%         g = g - lambda^2*penalty;
%     end
% end

% function f = funVect_diffpen(Phi,U,A,a,oneMat,W,lambda,L,z,K)
%     % D is the penalty specific matrix
%         % D is the penalty specific matrix
%     f = 0;
%     for j=1:length(Phi)
%         f = f+ (W(j)*log(dot(Phi(j,:),exp(U*A*a(j,:)'))) - W(j)*log(dot(oneMat(j,:),exp(U*A*a(j,:)'))));
%     end
%         
%     % apply penalization
%     if lambda>0
% %         penalty = lambda*(ones(1,M-2) * (sqrt((L*A).^2)*z));
%         penalty = lambda^2*(ones(1,M-2) * ((L*A).^2*z)); %removed sqrt
%         f = f - penalty;
%     end
% end

function gamma = tdxFit_static(xTrain, pps, hSpatial)
    % perform ML estimate for static KDX model

    Z = normpdf(xTrain,pps,repmat(hSpatial,1,length(pps)))';
    Z(Z==0)=1e-5;
    opt = optimoptions('fmincon','SpecifyObjectiveGradient',true,'Algorithm','interior-point','Display','notify');
    x0 = repmat(1/length(pps),1,length(pps));
    Aeq = ones(1,length(pps));
    beq = 1;
    lb = zeros(1,length(pps));
    ub = ones(1,length(pps));
    gamma = fmincon(@F,x0,[],[],Aeq,beq,lb,ub,[],opt);
    
    
    function [val,g] = F(x)
        % x is gamma vector
        val = -funVect(Z,x); % function value
        if nargout > 1
            g = -gradientVect(Z,x)'; % gradient
        end
    end
end

function val = funVect(Z,x)
    l = sum(log(x*Z));
    val = l;
end
function g = gradientVect(Z,x)
    a = x*Z;
    b = a.^-1;
    g = b*Z';
end

